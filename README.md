# docker-pypi-server

Host a pypi compatible server in a docker container. 

Can be started by:

`docker-compose up -d --build`
